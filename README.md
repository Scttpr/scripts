# List of different scripts

## Git-log

Tool to enhance `git log` display, allow to add other `git log` flags to the command :

```bash
# short one liner
git-log -s

# two liner display with more informations
git-log -l

# pretty raw history
git-log -r

# add existing flags
git-log -r --graph
```

## Rmnm

Simple tool to clear all node_modules recursively from a parent path :

```bash
# command
rmnm <path_to_delete_node_modules_from>

# example
rmnm .
rmnm /var/www/html
```