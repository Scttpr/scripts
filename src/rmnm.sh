if [[ $1 ]]; then
  echo -e "\e[0;33m Looking recursively for node_modules in '$1'... \e[0m"
  results=$(find "$1" -name 'node_modules');
  if [[ ! $results ]]; then
    echo -e "\e[0;32m No node_modules found in this directory \e[0m"
  else
    for path in $results
      do
        rm -rf "$path"
        echo -e "\e[0;32m deleted $path \e[0m"
      done
  fi
else
  echo -e "\e[0;31m Error: you must provide a directory path to the command \e[0m"
fi
