command="git log";
command_registered=0;
opts=();

increment_command()
{
  string=$1
  command="${command} ${string}"
  command_registered+=1
}

while [[ "$#" -gt 0 ]]; do
  if [[ $command_registered == 0 ]]; then
    case $1 in

      -s|--short)
        increment_command "--format='%Cred%h%Creset - %C(bold)%s%Creset %C(yellow)%D'"
        ;;
      -l|--long)
        increment_command "--format='%Cred%h%Creset - %C(bold green)%<(25,trunc)%ce%Creset %C(green)%cD%Creset%n%C(bold)%s%Creset %C(yellow)%d%Creset%n'"
        ;;
      -r|--raw)
        increment_command "--format='%C(red)Commit %C(bold)%H%Creset%nTree %T%nParents %P%n%C(cyan)Author %C(bold)%an <%ae>%Creset %C(cyan)%aD%Creset%n%C(green)Committer %C(bold)%cn <%ce>%Creset %C(green)%cD%Creset%nTitle %C(bold)%s%Creset%C(yellow)%d%n%b%n%N%n'"
        ;;
      *)
        opts+=("$1")
    esac
  else
    opts+=("$1")
  fi
  shift
done

for i in "${opts[@]}"; do increment_command "$i"; done

eval "$command";


